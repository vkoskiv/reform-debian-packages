commit bf83720d678eae161b36d01cbec481f648e809cf
Author: Lukas F. Hartmann <lukas@mntre.com>
Date:   Tue Apr 16 16:39:58 2024 +0200

    panel-jdi-lt070me05000: simplify and combine fixes for imx8mp and a311d for pocket reform; fix glitches by turning off properly.

--- a/drivers/gpu/drm/panel/panel-jdi-lt070me05000.c
+++ b/drivers/gpu/drm/panel/panel-jdi-lt070me05000.c
@@ -53,11 +53,8 @@ static int jdi_panel_init(struct jdi_pan
 
 	dsi->mode_flags |= MIPI_DSI_MODE_LPM;
 
-	ret = mipi_dsi_dcs_soft_reset(dsi);
-	if (ret < 0)
-		return ret;
-
-	usleep_range(10000, 20000);
+	msleep(20);
+	ret = mipi_dsi_dcs_write(dsi, MIPI_DCS_WRITE_CONTROL_DISPLAY, (u8[]) {0x2c}, 1);
 
 	ret = mipi_dsi_dcs_set_pixel_format(dsi, MIPI_DCS_PIXEL_FMT_24BIT << 4);
 	if (ret < 0) {
@@ -65,113 +62,59 @@ static int jdi_panel_init(struct jdi_pan
 		return ret;
 	}
 
-	ret = mipi_dsi_dcs_set_column_address(dsi, 0, jdi->mode->hdisplay - 1);
-	if (ret < 0) {
-		dev_err(dev, "failed to set column address: %d\n", ret);
-		return ret;
-	}
-
-	ret = mipi_dsi_dcs_set_page_address(dsi, 0, jdi->mode->vdisplay - 1);
-	if (ret < 0) {
-		dev_err(dev, "failed to set page address: %d\n", ret);
-		return ret;
-	}
-
-	/*
-	 * BIT(5) BCTRL = 1 Backlight Control Block On, Brightness registers
-	 *                  are active
-	 * BIT(3) BL = 1    Backlight Control On
-	 * BIT(2) DD = 0    Display Dimming is Off
-	 */
-	ret = mipi_dsi_dcs_write(dsi, MIPI_DCS_WRITE_CONTROL_DISPLAY,
-				 (u8[]){ 0x24 }, 1);
-	if (ret < 0) {
-		dev_err(dev, "failed to write control display: %d\n", ret);
-		return ret;
-	}
-
-	/* CABC off */
-	ret = mipi_dsi_dcs_write(dsi, MIPI_DCS_WRITE_POWER_SAVE,
-				 (u8[]){ 0x00 }, 1);
-	if (ret < 0) {
-		dev_err(dev, "failed to set cabc off: %d\n", ret);
-		return ret;
-	}
+	// write_memory_start
+	ret = mipi_dsi_generic_write(dsi, (u8[]) {0x2c}, 1);
+	ret = mipi_dsi_generic_write(dsi, (u8[]) {0x00}, 0);
 
+	msleep(200);
 	ret = mipi_dsi_dcs_exit_sleep_mode(dsi);
 	if (ret < 0) {
 		dev_err(dev, "failed to set exit sleep mode: %d\n", ret);
 		return ret;
 	}
 
-	msleep(120);
+	// required delay
+	msleep(800);
 
+	// MCAP off
 	ret = mipi_dsi_generic_write(dsi, (u8[]){0xB0, 0x00}, 2);
 	if (ret < 0) {
 		dev_err(dev, "failed to set mcap: %d\n", ret);
 		return ret;
 	}
 
-	mdelay(10);
+	// required delay
+	mdelay(200);
 
-	/* Interface setting, video mode */
+	// Interface setting, video mode
 	ret = mipi_dsi_generic_write(dsi, (u8[])
-				     {0xB3, 0x26, 0x08, 0x00, 0x20, 0x00}, 6);
+						 {0xB3, 0x14, 0x08, 0x00, 0x22, 0x00}, 6);
 	if (ret < 0) {
 		dev_err(dev, "failed to set display interface setting: %d\n"
 			, ret);
 		return ret;
 	}
 
+	// interface ID setting
 	mdelay(20);
+	ret = mipi_dsi_generic_write(dsi, (u8[]) {0xb4, 0x0c}, 2);
 
-	ret = mipi_dsi_generic_write(dsi, (u8[]){0xB0, 0x03}, 2);
-	if (ret < 0) {
-		dev_err(dev, "failed to set default values for mcap: %d\n"
-			, ret);
-		return ret;
-	}
-
-	return 0;
-}
-
-static int jdi_panel_on(struct jdi_panel *jdi)
-{
-	struct mipi_dsi_device *dsi = jdi->dsi;
-	struct device *dev = &jdi->dsi->dev;
-	int ret;
-
-	dsi->mode_flags |= MIPI_DSI_MODE_LPM;
+	// DSI control
+	ret = mipi_dsi_generic_write(dsi, (u8[]) {0xb6, 0x3a, 0xd3}, 3);
 
 	ret = mipi_dsi_dcs_set_display_on(dsi);
-	if (ret < 0)
-		dev_err(dev, "failed to set display on: %d\n", ret);
 
-	return ret;
+	return 0;
 }
 
-static void jdi_panel_off(struct jdi_panel *jdi)
+static int jdi_panel_disable(struct drm_panel *panel)
 {
-	struct mipi_dsi_device *dsi = jdi->dsi;
-	struct device *dev = &jdi->dsi->dev;
-	int ret;
-
-	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;
-
-	ret = mipi_dsi_dcs_set_display_off(dsi);
-	if (ret < 0)
-		dev_err(dev, "failed to set display off: %d\n", ret);
+	struct jdi_panel *jdi = to_jdi_panel(panel);
 
-	ret = mipi_dsi_dcs_enter_sleep_mode(dsi);
-	if (ret < 0)
-		dev_err(dev, "failed to enter sleep mode: %d\n", ret);
+	mipi_dsi_dcs_set_display_off(jdi->dsi);
+	mipi_dsi_dcs_enter_sleep_mode(jdi->dsi);
 
 	msleep(100);
-}
-
-static int jdi_panel_disable(struct drm_panel *panel)
-{
-	struct jdi_panel *jdi = to_jdi_panel(panel);
 
 	backlight_disable(jdi->backlight);
 
@@ -184,17 +127,15 @@ static int jdi_panel_unprepare(struct dr
 	struct device *dev = &jdi->dsi->dev;
 	int ret;
 
-	jdi_panel_off(jdi);
-
 	ret = regulator_bulk_disable(ARRAY_SIZE(jdi->supplies), jdi->supplies);
 	if (ret < 0)
 		dev_err(dev, "regulator disable failed, %d\n", ret);
 
 	gpiod_set_value(jdi->enable_gpio, 0);
 
-	gpiod_set_value(jdi->reset_gpio, 1);
+	if (!IS_ERR(jdi->reset_gpio)) gpiod_set_value(jdi->reset_gpio, 1);
 
-	gpiod_set_value(jdi->dcdc_en_gpio, 0);
+	if (!IS_ERR(jdi->dcdc_en_gpio)) gpiod_set_value(jdi->dcdc_en_gpio, 0);
 
 	return 0;
 }
@@ -213,54 +154,36 @@ static int jdi_panel_prepare(struct drm_
 
 	msleep(20);
 
-	gpiod_set_value(jdi->dcdc_en_gpio, 1);
+	if (!IS_ERR(jdi->dcdc_en_gpio)) gpiod_set_value(jdi->dcdc_en_gpio, 1);
 	usleep_range(10, 20);
 
-	gpiod_set_value(jdi->reset_gpio, 0);
+	if (!IS_ERR(jdi->reset_gpio)) gpiod_set_value(jdi->reset_gpio, 0);
 	usleep_range(10, 20);
 
 	gpiod_set_value(jdi->enable_gpio, 1);
 	usleep_range(10, 20);
 
-	ret = jdi_panel_init(jdi);
-	if (ret < 0) {
-		dev_err(dev, "failed to init panel: %d\n", ret);
-		goto poweroff;
-	}
-
-	ret = jdi_panel_on(jdi);
-	if (ret < 0) {
-		dev_err(dev, "failed to set panel on: %d\n", ret);
-		goto poweroff;
-	}
-
 	return 0;
-
-poweroff:
-	ret = regulator_bulk_disable(ARRAY_SIZE(jdi->supplies), jdi->supplies);
-	if (ret < 0)
-		dev_err(dev, "regulator disable failed, %d\n", ret);
-
-	gpiod_set_value(jdi->enable_gpio, 0);
-
-	gpiod_set_value(jdi->reset_gpio, 1);
-
-	gpiod_set_value(jdi->dcdc_en_gpio, 0);
-
-	return ret;
 }
 
 static int jdi_panel_enable(struct drm_panel *panel)
 {
 	struct jdi_panel *jdi = to_jdi_panel(panel);
+	struct device *dev = &jdi->dsi->dev;
+	int ret;
 
+	ret = jdi_panel_init(jdi);
+	if (ret < 0) {
+		dev_err(dev, "failed jdi_panel_init: %d\n", ret);
+		return ret;
+	}
 	backlight_enable(jdi->backlight);
 
 	return 0;
 }
 
 static const struct drm_display_mode default_mode = {
-		.clock = 155493,
+		.clock = 140000,
 		.hdisplay = 1200,
 		.hsync_start = 1200 + 48,
 		.hsync_end = 1200 + 48 + 32,
@@ -273,7 +196,7 @@ static const struct drm_display_mode def
 };
 
 static int jdi_panel_get_modes(struct drm_panel *panel,
-			       struct drm_connector *connector)
+						 struct drm_connector *connector)
 {
 	struct drm_display_mode *mode;
 	struct jdi_panel *jdi = to_jdi_panel(panel);
@@ -287,6 +210,15 @@ static int jdi_panel_get_modes(struct dr
 		return -ENOMEM;
 	}
 
+	// on A311D, we shift the vsync by one line to counteract VIU_OSD_HOLD_FIFO_LINES
+	if (of_property_present(dev->of_node, "vsync-shift")) {
+		uint32_t vsync_shift = 0;
+		of_property_read_u32(dev->of_node, "vsync-shift", &vsync_shift);
+		dev_warn(dev, "vsync-shift from device tree: %d\n", vsync_shift);
+		mode->vsync_start += vsync_shift;
+		mode->vsync_end += vsync_shift;
+	}
+
 	drm_mode_set_name(mode);
 
 	drm_mode_probed_add(connector, mode);
@@ -299,31 +231,20 @@ static int jdi_panel_get_modes(struct dr
 
 static int dsi_dcs_bl_get_brightness(struct backlight_device *bl)
 {
-	struct mipi_dsi_device *dsi = bl_get_data(bl);
-	int ret;
 	u16 brightness = bl->props.brightness;
 
-	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;
-
-	ret = mipi_dsi_dcs_get_display_brightness(dsi, &brightness);
-	if (ret < 0)
-		return ret;
-
-	dsi->mode_flags |= MIPI_DSI_MODE_LPM;
-
+	// FIXME kernel oops when getting brightness via DCS
 	return brightness & 0xff;
 }
 
 static int dsi_dcs_bl_update_status(struct backlight_device *bl)
 {
-	struct mipi_dsi_device *dsi = bl_get_data(bl);
-	int ret;
+	struct jdi_panel *jdi = bl_get_data(bl);
+	struct mipi_dsi_device *dsi = jdi->dsi;
 
 	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;
 
-	ret = mipi_dsi_dcs_set_display_brightness(dsi, bl->props.brightness);
-	if (ret < 0)
-		return ret;
+	mipi_dsi_dcs_set_display_brightness(dsi, bl->props.brightness);
 
 	dsi->mode_flags |= MIPI_DSI_MODE_LPM;
 
@@ -336,9 +257,9 @@ static const struct backlight_ops dsi_bl
 };
 
 static struct backlight_device *
-drm_panel_create_dsi_backlight(struct mipi_dsi_device *dsi)
+drm_panel_create_dsi_backlight(struct jdi_panel *jdi)
 {
-	struct device *dev = &dsi->dev;
+	struct device *dev = &jdi->dsi->dev;
 	struct backlight_properties props;
 
 	memset(&props, 0, sizeof(props));
@@ -346,8 +267,8 @@ drm_panel_create_dsi_backlight(struct mi
 	props.brightness = 255;
 	props.max_brightness = 255;
 
-	return devm_backlight_device_register(dev, dev_name(dev), dev, dsi,
-					      &dsi_bl_ops, &props);
+	return devm_backlight_device_register(dev, dev_name(dev), dev, jdi,
+								&dsi_bl_ops, &props);
 }
 
 static const struct drm_panel_funcs jdi_panel_funcs = {
@@ -389,15 +310,15 @@ static int jdi_panel_add(struct jdi_pane
 
 	jdi->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
 	if (IS_ERR(jdi->reset_gpio))
-		return dev_err_probe(dev, PTR_ERR(jdi->reset_gpio),
+		dev_err_probe(dev, PTR_ERR(jdi->reset_gpio),
 				     "cannot get reset-gpios %d\n", ret);
 
 	jdi->dcdc_en_gpio = devm_gpiod_get(dev, "dcdc-en", GPIOD_OUT_LOW);
 	if (IS_ERR(jdi->dcdc_en_gpio))
-		return dev_err_probe(dev, PTR_ERR(jdi->dcdc_en_gpio),
+		dev_err_probe(dev, PTR_ERR(jdi->dcdc_en_gpio),
 				     "cannot get dcdc-en-gpio %d\n", ret);
 
-	jdi->backlight = drm_panel_create_dsi_backlight(jdi->dsi);
+	jdi->backlight = drm_panel_create_dsi_backlight(jdi);
 	if (IS_ERR(jdi->backlight))
 		return dev_err_probe(dev, PTR_ERR(jdi->backlight),
 				     "failed to register backlight %d\n", ret);
@@ -423,8 +344,17 @@ static int jdi_panel_probe(struct mipi_d
 
 	dsi->lanes = 4;
 	dsi->format = MIPI_DSI_FMT_RGB888;
-	dsi->mode_flags =  MIPI_DSI_MODE_VIDEO_HSE | MIPI_DSI_MODE_VIDEO |
-			   MIPI_DSI_CLOCK_NON_CONTINUOUS;
+	dsi->mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_HSE;
+
+	// on a311d it works only without burst, but imx8mplus needs burst mode
+	if (of_property_present(dsi->dev.of_node, "burst-mode")) {
+		dsi->mode_flags |= MIPI_DSI_MODE_VIDEO_BURST;
+		dev_warn(&dsi->dev, "DSI burst mode enabled via device tree\n");
+	}
+	if (of_property_present(dsi->dev.of_node, "no-eot-hfp-hbp-hsa")) {
+		dsi->mode_flags |= MIPI_DSI_MODE_NO_EOT_PACKET | MIPI_DSI_MODE_VIDEO_NO_HFP | MIPI_DSI_MODE_VIDEO_NO_HBP | MIPI_DSI_MODE_VIDEO_NO_HSA;
+		dev_warn(&dsi->dev, "DSI eot/hfp/hbp/hsa disabled via device tree\n");
+	}
 
 	jdi = devm_kzalloc(&dsi->dev, sizeof(*jdi), GFP_KERNEL);
 	if (!jdi)
